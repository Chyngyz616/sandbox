package rivalAutotests;

import com.google.common.io.Resources;
import io.qameta.allure.Allure;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public class RivalBigCashWinEngine {
    WebDriver driver;
    public String generatedIDString;
    public int generatedIDInteger;
    public int idRangeA = 10000;
    public int idRangeB = 100000;
    Screen screen = new Screen();
    String systemPath = System.getProperty("user.dir") + "//";
    Pattern rivalBigCashWinStartButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647110094628.png");
    Pattern rivalBigCashWinBetButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647110255905.png").similar(0.1f);
    Pattern rivalBigCashWinAutoSpinSelectButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647111403895.png").similar(0.3f);
    Pattern rivalBigCashWinAutoSpin3SelectButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647111510279.png").similar(0.3f);
    Pattern rivalBigCashWinSettingsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647111536311.png");
    Pattern rivalBigCashWinChipsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647256947286.png");
    Pattern rivalBigCashWinSettings2Button = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647256970148.png");
    Pattern rivalBigCashWinFastButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647257853363.png");
    Pattern rivalBigCashWinLeftButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647257865514.png");
    Pattern rivalBigCashWinInfoButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647256984563.png");
    Pattern rivalBigCashWinCloseSettingsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647257032680.png");
    Pattern rivalBigCashWinLeftAutoSpinsSelectButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647321979443.png");
    Pattern rivalBigCashWinLeft3AutospinsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647322134851.png");


    public RivalBigCashWinEngine(WebDriver driver) {
        this.driver = driver;
    }

    public void rivalBigCashWinSmokeTesting () throws InterruptedException, FindFailed {
        driver.findElement(By.xpath("//span[@title='Choose Operator']")).click();
        driver.findElement(By.xpath("//input[@class='select2-search__field']")).sendKeys("rival", Keys.ENTER);
        Thread.sleep(1000);
        driver.findElement(By.xpath("//span[@title='Choose Game']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//input[@class='select2-search__field']")).sendKeys("rival-big-cash-win", Keys.ENTER);
        Thread.sleep(1000);
        driver.findElement(By.xpath("//span[@title='Choose TestCase']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//li[text()='Desktop real']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[text()='Launch']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//button[text()='Fullscreen']")).click();
        Thread.sleep(50000);
//        Pattern rivalBigCashWinStartButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647110094628.png");
//        screen.click(rivalBigCashWinStartButton);
//        Thread.sleep(1000);
//        Pattern rivalBigCashWinBetButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647110255905.png").similar(0.1f);
//        screen.click(rivalBigCashWinBetButton);
//        Thread.sleep(5000);
//        Pattern rivalBigCashWinAutoSpinSelectButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647111403895.png").similar(0.3f);
//        Thread.sleep(1000);
//        screen.click(rivalBigCashWinAutoSpinSelectButton);
//        Thread.sleep(500);
//        Pattern rivalBigCashWinAutoSpin3SelectButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647111510279.png").similar(0.3f);
//        screen.click(rivalBigCashWinAutoSpin3SelectButton);
//        Thread.sleep(15000);
//        Pattern rivalBigCashWinSettingsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647111536311.png");
//        screen.click(rivalBigCashWinSettingsButton);
//        Pattern rivalBigCashWinChipsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647256947286.png");
//        Thread.sleep(1500);
//        screen.click(rivalBigCashWinChipsButton);
//        Pattern rivalBigCashWinSettings2Button = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647256970148.png");
//        Thread.sleep(1500);
//        Pattern rivalBigCashWinFastButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647257853363.png");
//        screen.click(rivalBigCashWinSettings2Button);
//        Thread.sleep(1500);
//        screen.click(rivalBigCashWinFastButton);
//        Thread.sleep(500);
//        Pattern rivalBigCashWinLeftButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647257865514.png");
//        Thread.sleep(500);
//        screen.click(rivalBigCashWinLeftButton);
////        Pattern rivalBigCashWinInfoButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647256984563.png");
//        Thread.sleep(1500);
//        screen.click(rivalBigCashWinInfoButton);
//        Pattern rivalBigCashWinCloseSettingsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647257032680.png");
//        Thread.sleep(1500);
//        screen.click(rivalBigCashWinCloseSettingsButton);
//        Thread.sleep(1000);
//        Pattern rivalBigCashWinLeftAutoSpinsSelectButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647321979443.png");
//        Thread.sleep(500);
//        screen.click(rivalBigCashWinLeftAutoSpinsSelectButton);
//        Thread.sleep(500);
//        Pattern rivalBigCashWinLeft3AutospinsButton = new Pattern(systemPath + "src/test/resources/rivalUIElements/1647322134851.png");
//        screen.click(rivalBigCashWinLeft3AutospinsButton);
//        Thread.sleep(15000);
    }

    public void startGame () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinStartButton);
        Thread.sleep(1000);
    }

    public void bet () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinBetButton);
        Thread.sleep(5000);
    }

    public void autoSpinsSelect () throws InterruptedException, FindFailed {
        screen.click(rivalBigCashWinAutoSpinSelectButton);
        Thread.sleep(1000);
    }

    public void autoSpin3Select () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinAutoSpin3SelectButton);
        Thread.sleep(15000);
    }

    public void settings () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinSettingsButton);
        Thread.sleep(1500);
    }

    public void chips () throws InterruptedException, FindFailed {
        Thread.sleep(1500);
        screen.click(rivalBigCashWinChipsButton);
    }

    public void settings2 () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinSettings2Button);
        Thread.sleep(1500);
    }

    public void fast () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinFastButton);
        Thread.sleep(500);
    }

    public void leftButton () throws InterruptedException, FindFailed {
        screen.click(rivalBigCashWinLeftButton);
        Thread.sleep(500);
    }

    public void info () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinInfoButton);
        Thread.sleep(1500);
    }

    public void closeSettings () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinCloseSettingsButton);
        Thread.sleep(1500);
    }

    public void leftAutoSpinsSelect () throws InterruptedException, FindFailed {
        screen.click(rivalBigCashWinLeftAutoSpinsSelectButton);
        Thread.sleep(500);
    }

    public void leftAutoSpinsSelect3 () throws FindFailed, InterruptedException {
        screen.click(rivalBigCashWinLeft3AutospinsButton);
        Thread.sleep(15000);
    }
}
