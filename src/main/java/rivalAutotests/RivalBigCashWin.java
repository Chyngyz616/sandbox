package rivalAutotests;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.driver.DriverFactory;

import java.io.ByteArrayInputStream;

import static util.PropertyFileReader.getProperty;
import static util.driver.DriverHolder.getDriver;
import static util.driver.DriverHolder.setDriver;

public class RivalBigCashWin {
    WebDriver driver;
    RivalBigCashWinEngine rivalBigCashWinEngine;

    @BeforeMethod
    public void before() {
        setDriver(DriverFactory.getNewDriverInstance(getProperty("browser")));
        getDriver().manage().window().maximize();
        getDriver().get("https://staging.gamehub.zone/admin/games");
        getDriver().findElement(By.xpath("//input[@type='text']")).sendKeys("chingiz");
        getDriver().findElement(By.xpath("//input[@type='password']")).sendKeys("ibYswCvtEJ3");
        getDriver().findElement(By.xpath("//button[@class='btn btn-lg btn-primary btn-block']")).click();
        getDriver().findElement(By.xpath("//a[text()='Quick game launch']")).click();
    }

    @Test
    public void rivalBigCashWinSmokeTesting () throws FindFailed, InterruptedException {
        rivalBigCashWinEngine = new RivalBigCashWinEngine(getDriver());
        rivalBigCashWinEngine.rivalBigCashWinSmokeTesting();
        rivalBigCashWinSmokeTestingStartGame();
        rivalBigCashWinSmokeTestingBet();
        rivalBigCashWinSmokeTestingAutoSpinSelect();
        rivalBigCashWinSmokeTestingAutoSpinSelect3();
        rivalBigCashWinSmokeTestingSettings();
        rivalBigCashWinSmokeTestingChips();
        rivalBigCashWinSmokeTestingSettings2();
        rivalBigCashWinSmokeTestingFast();
        rivalBigCashWinSmokeTestingLeft();
        rivalBigCashWinSmokeTestingInfo();
        rivalBigCashWinSmokeTestingCloseSettings();
        rivalBigCashWinSmokeTestingLeftAutoSpinsSelect();
        rivalBigCashWinSmokeTestingLeftAutoSpins3();
    }
    @Step("Start the Rival Big Cash Win")
    public void rivalBigCashWinSmokeTestingStartGame () throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.startGame();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Bet")
    public void rivalBigCashWinSmokeTestingBet () throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.bet();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on autospin button")
    public void rivalBigCashWinSmokeTestingAutoSpinSelect() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.autoSpinsSelect();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on 3 button")
    public void rivalBigCashWinSmokeTestingAutoSpinSelect3() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.autoSpin3Select();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on settings button")
    public void rivalBigCashWinSmokeTestingSettings() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.settings();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the chips button")
    public void rivalBigCashWinSmokeTestingChips() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.chips();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on settings button")
    public void rivalBigCashWinSmokeTestingSettings2() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.settings2();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the fast button")
    public void rivalBigCashWinSmokeTestingFast() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.fast();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the left button")
    public void rivalBigCashWinSmokeTestingLeft() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.leftButton();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the info button")
    public void rivalBigCashWinSmokeTestingInfo() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.info();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the close settings button")
    public void rivalBigCashWinSmokeTestingCloseSettings() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.closeSettings();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the left autospins select button")
    public void rivalBigCashWinSmokeTestingLeftAutoSpinsSelect() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.leftAutoSpinsSelect();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @Step("Click on the 3")
    public void rivalBigCashWinSmokeTestingLeftAutoSpins3() throws FindFailed, InterruptedException {
        rivalBigCashWinEngine.leftAutoSpinsSelect3();
        Allure.addAttachment("BigCashWinScreenshot", new ByteArrayInputStream(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES)));
    }
    @AfterMethod
    public void after() {
        if (getDriver() != null) {
            getDriver().quit();
        }
    }
}
